from flask import request
from Models.Schema import tag_schema, tags_schema
from Models.Tag import Tag
from flask_restful import Resource, abort
from app import db
from werkzeug.security import generate_password_hash
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager, current_user

class TagListResource(Resource):
    @jwt_required()
    def get(self):
        tags = Tag.query.all()
        return tags_schema.dump(tags)

    @jwt_required()
    def post(self):
        new_tag = Tag(
            name=request.json['name']
        )
        db.session.add(new_tag)
        db.session.commit()
        return tag_schema.dump(new_tag)