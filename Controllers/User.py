from flask import request
from Models.User import User
from Models.Schema import user_schema, users_schema
from flask_restful import Resource, abort
from app import db
from werkzeug.security import generate_password_hash
from flask_jwt_extended import create_access_token, get_jwt_identity, jwt_required, JWTManager, current_user

class UserListResource(Resource):
    @jwt_required()
    def get(self):
        users = User.query.all()
        return users_schema.dump(users)

    @jwt_required()
    def post(self):
        new_user = User(
            name=request.json['name'],
            email=request.json['email'],
            password=generate_password_hash(request.json['password'])
        )
        db.session.add(new_user)
        db.session.commit()
        return user_schema.dump(new_user)


class UserResource(Resource):
    @jwt_required()
    def get(self, user_id):
        user = User.query.get_or_404(user_id)
        return user_schema.dump(post)

    @jwt_required()
    def put(self, user_id):
        user = User.query.get_or_404(user_id)

        user.name = request.json['name']
        user.email = request.json['email']
        user.password = generate_password_hash(request.json['password'])

        db.session.commit()
        return user_schema.dump(post)

    @jwt_required()
    def patch(self, user_id):
        user = User.query.get_or_404(user_id)

        if 'name' in request.json:
            user.name = request.json['name']
        if 'email' in request.json:
            user.email = request.json['email']
        if 'password' in request.json:
            user.password = generate_password_hash(request.json['password'])

        db.session.commit()
        return user_schema.dump(post)

    @jwt_required()
    def delete(self, user_id):
        user = User.query.get_or_404(user_id)
        db.session.delete(user)
        db.session.commit()
        return '', 204