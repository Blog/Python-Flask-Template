from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from Models.User import User
from Models.Post import Post
from Models.Tag import Tag

class UserSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = User
        exclude = ("password",)
        include_fk = True
        include_relationships = True
        load_instance = True

class PostSchema(SQLAlchemyAutoSchema):
    class Meta:
        model= Post
        include_fk = True
        include_relationships = True
        load_instance = True

class TagSchema(SQLAlchemyAutoSchema):
    class Meta:
        model= Tag
        include_fk = True
        include_relationships = True
        load_instance = True

user_schema = UserSchema()
users_schema = UserSchema(many=True)
post_schema = PostSchema()
posts_schema = PostSchema(many=True)
tag_schema = TagSchema()
tags_schema = TagSchema(many=True)