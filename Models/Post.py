from app import db, ma
from Models.User import User
from Models.Tag import Tag

tags_posts = db.Table('tags_posts_mapping',
    db.Column('tag_id', db.Integer, db.ForeignKey('tags.id'), primary_key=True),
    db.Column('post_id', db.Integer, db.ForeignKey('post.id'), primary_key=True)
)


class Post(db.Model):
    __tablename__ = "post"
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(50))
    content = db.Column(db.String(255))
    tags = db.relationship('Tag', secondary=tags_posts, lazy='subquery',
        backref=db.backref('posts', lazy=True))
    author_id = db.Column(db.Integer, db.ForeignKey("user.id"))
    author = db.relationship("User", backref="posts")

    def __repr__(self):
        return '<Post %s>' % self.title

