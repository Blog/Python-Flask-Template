from app import db, ma

class Tag(db.Model):
    __tablename__ = "tags"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))


    def __repr__(self):
        return '<Tag %s>' % self.name


