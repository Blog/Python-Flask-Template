from app import db, ma
from werkzeug.security import check_password_hash

class User(db.Model):
    __tablename__ = "user"
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    email = db.Column(db.String(255))
    password = db.Column(db.String(255))

    def __repr__(self):
        return '<User %s>' % self.name

    def check_password(self, password):
        return check_password_hash(self.password, password)

